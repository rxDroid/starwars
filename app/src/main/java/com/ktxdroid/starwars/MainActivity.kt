package com.ktxdroid.starwars

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.ktxdroid.starwars.ui.navigation.HomeRoute
import com.ktxdroid.starwars.ui.navigation.StarWarsTopBar
import com.ktxdroid.starwars.ui.navigation.homeScreen
import com.ktxdroid.starwars.ui.navigation.rememberScreenBarState
import com.ktxdroid.starwars.ui.screens.details.detailsScreen
import com.ktxdroid.starwars.ui.screens.details.navigateToDetails
import com.ktxdroid.starwars.ui.screens.discover.discoverScreen
import com.ktxdroid.starwars.ui.screens.discover.navigateToDiscover
import com.ktxdroid.starwars.ui.theme.StarWarsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            StarWarsTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    val screenBarState = rememberScreenBarState(navController = navController)
                    Scaffold(
                        topBar = {
                            StarWarsTopBar(screenBarState = screenBarState)
                        },
                    ) { innerPadding ->
                        NavHost(
                            navController = navController,
                            startDestination = HomeRoute,
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(innerPadding)
                                .consumeWindowInsets(innerPadding)
                        ) {
                            homeScreen(
                                onDiscoverClicked = { type ->
                                    navController.navigateToDiscover(type)
                                }
                            )
                            discoverScreen(
                                screenBarState = screenBarState,
                                onItemClicked = {
                                    navController.navigateToDetails(it.name)
                                },
                                onBackClick = {
                                    navController.navigateUp()
                                }
                            )
                            detailsScreen(
                                screenBarState = screenBarState,
                                onCloseClick = {
                                    navController.popBackStack(
                                        route = HomeRoute,
                                        inclusive = false
                                    )
                                }
                            )
                        }
                    }
                }
            }
        }
    }
}
