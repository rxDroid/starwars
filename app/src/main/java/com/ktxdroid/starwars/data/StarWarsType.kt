package com.ktxdroid.starwars.data

enum class StarWarsType(val value: String) {

    MOVIES("films"),
    PEOPLE("people"),
    PLANETS("planets"),
    SPECIES("species"),
    STAR_SHIPS("starships"),
    VEHICLES("vehicles")
}
