package com.ktxdroid.starwars.data

import com.ktxdroid.starwars.api.StarWarsApiService
import com.ktxdroid.starwars.api.data.PlanetData
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

/**
 * Only one repository for this show case.
 * In a more complex scenario that would be a UseCase containing multiple repositories.
 * Could be a discussion worth to create a repository for each [StarWarsType].
 */
class PlanetsRepository(
    private val apiService: StarWarsApiService,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    //Only memory cache to keep it simple
    private var localMemoryCache: HashMap<String, PlanetData> = HashMap()
    private var nextPlanetsUrl: String = ""

    fun loadPlanets(isLoadingMore: Boolean) = flow<Response<List<PlanetData>>> {
        val planets = if (isLoadingMore && nextPlanetsUrl.isNotEmpty()) {
            val pageIndex = nextPlanetsUrl.last().toString()
            val planetsResponse = if (pageIndex.isNotEmpty()) {
                apiService.getMorePlanets(pageIndex)
            } else {
                null
            }
            if (planetsResponse != null) {
                nextPlanetsUrl = planetsResponse.next ?: ""
            }
            planetsResponse?.results ?: emptyList()
        } else {
            val planetsResponse = apiService.getPlanets()
            nextPlanetsUrl = planetsResponse.next ?: ""
            planetsResponse.results
        }
        planets.forEach {
            localMemoryCache[it.name] = it
        }
        emit(Response.Success(localMemoryCache.values.toList()))
    }.catch {
        emit(Response.Failure(it))
    }.flowOn(dispatcher)

    fun planetDetails(planetTitle: String?): PlanetData? = localMemoryCache[planetTitle]
}
