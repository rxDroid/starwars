package com.ktxdroid.starwars.data


sealed interface Response<out T> {

    data class Success<T>(
        val data: T
    ) : Response<T>

    data class Failure(
        val throwable: Throwable,
    ) : Response<Nothing>
}
