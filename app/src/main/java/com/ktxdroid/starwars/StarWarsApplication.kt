package com.ktxdroid.starwars

import android.app.Application
import com.ktxdroid.starwars.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class StarWarsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@StarWarsApplication)
            androidLogger()
            modules(appModule)
        }
    }
}
