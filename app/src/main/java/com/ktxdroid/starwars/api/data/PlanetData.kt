package com.ktxdroid.starwars.api.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PlanetData(
    val climate: String,
    val diameter: String,
    val films: List<String>,
    val gravity: String,
    val name: String,
    @SerialName("orbital_period")
    val orbitalPeriod: String,
    val population: String,
    val residents: List<String>,
    @SerialName("rotation_period")
    val rotationPeriod: String,
    @SerialName("surface_water")
    val surfaceWater: String,
    val terrain: String,
    val url: String
) {
    companion object {
        val INITIAL = PlanetData(
            climate = "",
            diameter = "",
            films = emptyList(),
            gravity = "",
            name = "",
            orbitalPeriod = "",
            population = "",
            residents = emptyList(),
            rotationPeriod = "",
            surfaceWater = "",
            terrain = "",
            url = ""
        )
    }
}
