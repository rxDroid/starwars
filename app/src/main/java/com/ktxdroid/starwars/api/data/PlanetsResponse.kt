package com.ktxdroid.starwars.api.data

import kotlinx.serialization.Serializable

@Serializable
data class PlanetsResponse(
    val count: Int,
    val next: String? = null,
    val previous: String? = null,
    val results: List<PlanetData>
)