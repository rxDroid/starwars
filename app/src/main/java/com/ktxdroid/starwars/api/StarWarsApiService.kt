package com.ktxdroid.starwars.api

import com.ktxdroid.starwars.api.data.PlanetsResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface StarWarsApiService {

    @GET("planets/")
    suspend fun getPlanets(): PlanetsResponse

    @GET("planets/")
    suspend fun getMorePlanets(@Query("page") pageIndex: String): PlanetsResponse

}
