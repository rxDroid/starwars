package com.ktxdroid.starwars.di

import com.ktxdroid.starwars.api.StarWarsApiService
import com.ktxdroid.starwars.data.PlanetsRepository
import com.ktxdroid.starwars.ui.screens.details.DetailsViewModel
import com.ktxdroid.starwars.ui.screens.discover.DiscoverViewModel
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.kotlinx.serialization.asConverterFactory

val appModule = module {

    viewModel { DiscoverViewModel(savedStateHandle = get(), repository = get()) }

    viewModel { DetailsViewModel(savedStateHandle = get(), repository = get()) }

    single { PlanetsRepository(apiService = get()) }

    single {
        val networkJson = Json { ignoreUnknownKeys = true }
        val retrofit = Retrofit.Builder()
            .baseUrl("https://swapi.dev/api/")
            .addConverterFactory(
                networkJson.asConverterFactory("application/json; charset=UTF8".toMediaType())
            )
            .build()

        retrofit.create(StarWarsApiService::class.java)
    }
}
