package com.ktxdroid.starwars.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.navigation.NavController
import com.ktxdroid.starwars.util.capitalizeTitle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class ScreenBarState(
    navController: NavController,
    scope: CoroutineScope,
) {

    init {
        navController.currentBackStackEntryFlow
            .distinctUntilChanged()
            .onEach { backStackEntry ->
                val screenTitle = backStackEntry.arguments?.getString("starWarsType")
                val route = backStackEntry.destination.route
                currentScreen = screenForRoute(route, screenTitle?.capitalizeTitle())
            }
            .launchIn(scope)
    }

    var currentScreen by mutableStateOf<Screen?>(null)
        private set

    val navigationIcon: ImageVector?
        @Composable get() = currentScreen?.topBarNavigationIcon

    val navigationIconContentDescription: String?
        @Composable get() = currentScreen?.navigationIconContentDescription

    val onNavigationIconClick: (() -> Unit)?
        @Composable get() = currentScreen?.onTopBarNavigationClick

    val title: String
        @Composable get() = currentScreen?.topBarTitle.orEmpty()

}

@Composable
fun rememberScreenBarState(
    navController: NavController,
    scope: CoroutineScope = rememberCoroutineScope(),
) = remember { ScreenBarState(navController, scope) }
