package com.ktxdroid.starwars.ui.screens.discover

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import com.ktxdroid.starwars.ui.compose.EndlessLazyColumn
import com.ktxdroid.starwars.ui.compose.FullScreenError
import com.ktxdroid.starwars.ui.navigation.Screen
import com.ktxdroid.starwars.ui.navigation.Screen.DiscoverScreen.AppBarIcons
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun DiscoverScreen(
    screenState: Screen.DiscoverScreen?,
    errorMessage: String? = null,
    planetItems: List<DiscoverItemData>,
    isLoading: Boolean,
    loadMore: () -> Unit,
    onItemClicked: (DiscoverItemData) -> Unit,
    onBackClick: () -> Unit,
) {

    LaunchedEffect(key1 = screenState) {
        screenState?.topBarButtons?.onEach { button ->
            when (button) {
                AppBarIcons.Back -> onBackClick()
            }
        }?.launchIn(this)
    }

    when {
        errorMessage != null -> FullScreenError(errorMessage = errorMessage)
        planetItems.isEmpty() -> Text(text = "Not implemented")
        else -> {

            EndlessLazyColumn(
                loading = isLoading,
                items = planetItems,
                itemKey = { it.name },
                itemContent = { item ->
                    DiscoverListItemView(
                        itemData = item,
                        onItemClick = onItemClicked
                    )
                },
                loadingItem = { DiscoverLoadingItemView() },
                loadMoreItems = loadMore
            )
        }
    }
}
