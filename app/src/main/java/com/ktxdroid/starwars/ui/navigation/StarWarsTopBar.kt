package com.ktxdroid.starwars.ui.navigation

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun StarWarsTopBar(
    screenBarState: ScreenBarState,
    modifier: Modifier = Modifier,
) {
    TopAppBar(
        modifier = modifier,
        colors = TopAppBarDefaults.topAppBarColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer,
        ),
        navigationIcon = {
            val icon = screenBarState.navigationIcon
            if (icon != null) {
                val callback = screenBarState.onNavigationIconClick
                IconButton(onClick = { callback?.invoke() }) {
                    Icon(
                        imageVector = icon,
                        contentDescription = screenBarState.navigationIconContentDescription
                    )
                }
            }
        },
        title = {
            val title = screenBarState.title
            if (title.isNotEmpty()) {
                Text(
                    text = title
                )
            }
        },
    )
}
