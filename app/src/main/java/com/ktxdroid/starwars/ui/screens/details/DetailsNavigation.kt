package com.ktxdroid.starwars.ui.screens.details

import androidx.compose.runtime.getValue
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.ktxdroid.starwars.ui.navigation.DetailsRoute
import com.ktxdroid.starwars.ui.navigation.DiscoverRoute
import com.ktxdroid.starwars.ui.navigation.Screen
import com.ktxdroid.starwars.ui.navigation.ScreenBarState
import org.koin.androidx.compose.koinViewModel

fun NavController.navigateToDetails(
    planetTitle: String,
    navOptions: NavOptions? = null
) {
    this.navigate("$DetailsRoute/$planetTitle", navOptions)
}

fun NavGraphBuilder.detailsScreen(
    screenBarState: ScreenBarState,
    onCloseClick: () -> Unit,
) {
    composable(
        route = "$DetailsRoute/{planetTitle}",
        arguments = listOf(navArgument("planetTitle") { type = NavType.StringType })
    ) {
        val screenState = screenBarState.currentScreen as? Screen.DetailsScreen
        val detailsViewModel: DetailsViewModel = koinViewModel()
        val planetData by detailsViewModel.planetState.collectAsStateWithLifecycle()
        DetailsScreen(
            screenState = screenState,
            planetData = planetData,
            onCloseClick = onCloseClick
        )
    }
}
