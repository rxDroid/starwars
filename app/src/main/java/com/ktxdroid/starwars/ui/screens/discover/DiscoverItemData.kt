package com.ktxdroid.starwars.ui.screens.discover

data class DiscoverItemData(
    val name: String
)
