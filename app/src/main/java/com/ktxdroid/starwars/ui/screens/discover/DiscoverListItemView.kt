package com.ktxdroid.starwars.ui.screens.discover

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun DiscoverListItemView(
    itemData: DiscoverItemData,
    onItemClick: (DiscoverItemData) -> Unit,
    modifier: Modifier = Modifier,
) {
    OutlinedCard {
        Row(
            modifier = modifier
                .fillMaxWidth()
                .clickable { onItemClick(itemData) }
                .padding(8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(text = itemData.name)
        }
    }
}
