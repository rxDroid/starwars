package com.ktxdroid.starwars.ui.screens.home

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.ktxdroid.starwars.data.StarWarsType

@Composable
fun HomeScreen(
    onClick: (StarWarsType) -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text("Your awesome Star Wars universe! Search for everything you are interested in.")
        StarWarsType.entries.forEach { type ->
            Button(onClick = { onClick(type) }) {
                Text(text = type.name)
            }
        }
    }
}
