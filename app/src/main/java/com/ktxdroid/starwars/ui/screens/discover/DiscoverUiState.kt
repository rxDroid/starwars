package com.ktxdroid.starwars.ui.screens.discover

data class DiscoverUiState(
    val isLoading: Boolean = false,
    val discoverItems: List<DiscoverItemData> = emptyList(),
    val error: String? = null
)
