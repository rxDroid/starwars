package com.ktxdroid.starwars.ui.navigation

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.ui.graphics.vector.ImageVector
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow

sealed interface Screen {

    val route: String
    val topBarTitle: String
    val topBarNavigationIcon: ImageVector?
    val navigationIconContentDescription: String?
    val onTopBarNavigationClick: (() -> Unit)?

    class HomeScreen(
        override val route: String = HomeRoute,
        override val topBarTitle: String = "Star Wars",
        override val topBarNavigationIcon: ImageVector? = null,
        override val navigationIconContentDescription: String? = null,
        override val onTopBarNavigationClick: (() -> Unit)? = null
    ) : Screen

    class DiscoverScreen(
        override val topBarTitle: String
    ) : Screen {

        private val _topBarButtons = MutableSharedFlow<AppBarIcons>(extraBufferCapacity = 1)
        val topBarButtons: Flow<AppBarIcons> = _topBarButtons.asSharedFlow()

        override val route: String = DiscoverRoute
        override val topBarNavigationIcon: ImageVector = Icons.AutoMirrored.Filled.ArrowBack
        override val navigationIconContentDescription: String = "Back"
        override val onTopBarNavigationClick: (() -> Unit) = {
            _topBarButtons.tryEmit(AppBarIcons.Back)
        }

        enum class AppBarIcons {
            Back
        }
    }

    class DetailsScreen : Screen {

        private val _topBarButtons = MutableSharedFlow<AppBarIcons>(extraBufferCapacity = 1)
        val topBarButtons: Flow<AppBarIcons> = _topBarButtons.asSharedFlow()

        override val route: String = DetailsRoute
        override val topBarTitle: String = "Details"
        override val topBarNavigationIcon: ImageVector = Icons.Default.Close
        override val navigationIconContentDescription: String = "Close"
        override val onTopBarNavigationClick: (() -> Unit) = {
            _topBarButtons.tryEmit(AppBarIcons.Close)
        }

        enum class AppBarIcons {
            Close
        }
    }
}
