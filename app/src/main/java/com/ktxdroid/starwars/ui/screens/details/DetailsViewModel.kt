package com.ktxdroid.starwars.ui.screens.details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.ktxdroid.starwars.api.data.PlanetData
import com.ktxdroid.starwars.data.PlanetsRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class DetailsViewModel(
    savedStateHandle: SavedStateHandle,
    repository: PlanetsRepository
) : ViewModel() {

    private val planetTitle: String? = savedStateHandle["planetTitle"]

    private val _planetState = MutableStateFlow(PlanetData.INITIAL)
    val planetState: StateFlow<PlanetData> = _planetState.asStateFlow()

    init {
        val planetDetails = repository.planetDetails(planetTitle)
        planetDetails?.let {
            _planetState.value = it
        }
    }
}
