package com.ktxdroid.starwars.ui.screens.details

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.ktxdroid.starwars.api.data.PlanetData
import com.ktxdroid.starwars.ui.navigation.Screen
import com.ktxdroid.starwars.ui.navigation.Screen.DetailsScreen.AppBarIcons
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun DetailsScreen(
    screenState: Screen.DetailsScreen?,
    planetData: PlanetData,
    onCloseClick: () -> Unit,
) {

    LaunchedEffect(key1 = screenState) {
        screenState?.topBarButtons?.onEach { button ->
            when (button) {
                //Close makes here actually no sense, just for demonstration purpose
                AppBarIcons.Close -> onCloseClick()
            }
        }?.launchIn(this)
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(4.dp)
    ) {
        Row {
            Text(text = "Name: ")
            Text(text = planetData.name)
        }
        if (planetData.films.isNotEmpty()) {
            Row {
                Text(text = "Movies: ")
                Column {
                    planetData.films.forEach {
                        Text(text = it)
                    }
                }
            }
        }
        if (planetData.residents.isNotEmpty()) {
            Row {
                Text(text = "Residents: ")
                Column {
                    planetData.residents.forEach {
                        Text(text = it)
                    }
                }
            }
        }
        Row {
            Text(text = "Diameter: ")
            Text(text = planetData.diameter)
        }
        Row {
            Text(text = "Gravity: ")
            Text(text = planetData.gravity)
        }
        Row {
            Text(text = "Climate: ")
            Text(text = planetData.climate)
        }
        Row {
            Text(text = "OrbitalPeriod: ")
            Text(text = planetData.orbitalPeriod)
        }
        Row {
            Text(text = "Population: ")
            Text(text = planetData.population)
        }
        Row {
            Text(text = "RotationPeriod: ")
            Text(text = planetData.rotationPeriod)
        }
        Row {
            Text(text = "Surface Water: ")
            Text(text = planetData.surfaceWater)
        }
        Row {
            Text(text = "Terrain: ")
            Text(text = planetData.terrain)
        }
    }
}
