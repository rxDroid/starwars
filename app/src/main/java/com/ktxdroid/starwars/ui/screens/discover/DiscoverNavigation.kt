package com.ktxdroid.starwars.ui.screens.discover

import androidx.compose.runtime.getValue
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.ktxdroid.starwars.data.StarWarsType
import com.ktxdroid.starwars.ui.navigation.DiscoverRoute
import com.ktxdroid.starwars.ui.navigation.Screen
import com.ktxdroid.starwars.ui.navigation.ScreenBarState
import org.koin.androidx.compose.koinViewModel

fun NavController.navigateToDiscover(starWarsType: StarWarsType, navOptions: NavOptions? = null) {
    this.navigate("$DiscoverRoute/${starWarsType.name}", navOptions)
}

fun NavGraphBuilder.discoverScreen(
    screenBarState: ScreenBarState,
    onItemClicked: (DiscoverItemData) -> Unit,
    onBackClick: () -> Unit,
) {
    composable(
        route = "$DiscoverRoute/{starWarsType}",
        arguments = listOf(navArgument("starWarsType") { type = NavType.StringType })
    ) {
        val screenState = screenBarState.currentScreen as? Screen.DiscoverScreen
        val discoverViewModel: DiscoverViewModel = koinViewModel()
        val uiState by discoverViewModel.uiState.collectAsStateWithLifecycle()
        DiscoverScreen(
            screenState = screenState,
            loadMore = discoverViewModel::loadMore,
            planetItems = uiState.discoverItems,
            isLoading = uiState.isLoading,
            onItemClicked = onItemClicked,
            onBackClick = onBackClick
        )
    }
}
