package com.ktxdroid.starwars.ui.navigation

const val HomeRoute = "home"
const val DiscoverRoute = "discoverRoute"
const val DetailsRoute = "details"
fun screenForRoute(route: String?, screenTitle: String?): Screen = when {
    route?.startsWith(HomeRoute) == true -> Screen.HomeScreen()

    route?.startsWith(DiscoverRoute) == true -> Screen.DiscoverScreen(
        topBarTitle = screenTitle ?: ""
    )

    route?.startsWith(DetailsRoute) == true -> Screen.DetailsScreen()

    else -> throw NotImplementedError("This screen is missing!")
}
