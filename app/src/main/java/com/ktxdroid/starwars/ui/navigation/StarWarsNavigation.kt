package com.ktxdroid.starwars.ui.navigation

import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.ktxdroid.starwars.data.StarWarsType
import com.ktxdroid.starwars.ui.screens.home.HomeScreen

fun NavGraphBuilder.homeScreen(
    onDiscoverClicked: (StarWarsType) -> Unit
) {
    composable(HomeRoute) {
        HomeScreen(
            onClick = onDiscoverClicked
        )
    }
}
