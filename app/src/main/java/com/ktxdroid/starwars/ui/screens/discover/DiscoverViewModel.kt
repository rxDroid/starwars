package com.ktxdroid.starwars.ui.screens.discover

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ktxdroid.starwars.api.data.PlanetData
import com.ktxdroid.starwars.data.PlanetsRepository
import com.ktxdroid.starwars.data.Response
import com.ktxdroid.starwars.data.StarWarsType
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class DiscoverViewModel(
    savedStateHandle: SavedStateHandle,
    private val repository: PlanetsRepository
) : ViewModel() {

    private val starWars: String? = savedStateHandle["starWarsType"]

    private val _uiState = MutableStateFlow(DiscoverUiState())
    val uiState: StateFlow<DiscoverUiState> = _uiState.asStateFlow()

    init {
        val starWarsType = StarWarsType.entries.first { it.name == starWars }
        when (starWarsType) {
            StarWarsType.PLANETS -> {
                loadPlanets(false)
            }

            else -> {
                //Won't do, just a show case
            }
        }
    }

    fun loadMore() {
        loadPlanets(true)
    }

    private fun loadPlanets(isLoadingMore: Boolean) {
        viewModelScope.launch {
            repository.loadPlanets(isLoadingMore)
                .onStart {
                    _uiState.update {
                        it.copy(isLoading = true)
                    }
                }
                .collectLatest { response ->
                    handlerResponse(response)
                }
        }
    }

    private fun handlerResponse(response: Response<List<PlanetData>>) {
        when (response) {
            is Response.Failure -> {
                _uiState.update {
                    it.copy(
                        error = "Something went somewhere terribly wrong: " + response.throwable,
                        isLoading = false,
                        discoverItems = emptyList()
                    )
                }
            }

            is Response.Success -> {
                _uiState.update {
                    it.copy(
                        error = null,
                        isLoading = false,
                        discoverItems = response.data.map { data ->
                            DiscoverItemData(name = data.name)
                        }
                    )
                }
            }
        }
    }
}
