package com.ktxdroid.starwars.util

fun String.capitalizeTitle(): String {
    val smallWord = this.lowercase()
    return smallWord.replaceFirstChar(Char::uppercaseChar)
}
