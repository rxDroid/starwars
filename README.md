# StarWars - Coding Challenge

## My approach to tackle the coding challenge 
- [Reading the documentation](https://swapi.dev/documentation#base)
- Playing with Postman and making some test API calls to see if responses match documentation
- Setting up base Android project 
- make some coffee
- Setting up base project structure (DI, navigation, packages)
- thinking about UI/UX - how would I like to discover a Star War app?

## Used tools
- Android Studio Iguana
- Postman

## Working but not completed features
- [x] App is runnable in emulator
- [x] Three screens: Main, Discover and Details
- [x] Loading planets (including pagination)
- [x] Show planet details

## Open/skipped tasks (bc of running out of time)
- [ ] Localization
- [ ] Tag texts for ScreenReader
- [ ] Proguard config
- [ ] Theming and styling
- [ ] Unit tests
- [ ] Branching structure - only main branch. For a real project, I would use feature branches and a develop branch and not only a main branch.

## Struggles
- Backend makes circular navigation in app possible (when using links)
- Backend does not provide ids for single objects
- I'm not happy with TopBar and logic for pagination